// Express JS Server

// set up dependencies
const express = require("express");
const mongoose = require("mongoose");
const taskRoute = require("./routes/taskRoutes");

// server setup
const app = express();
const port = 3001;

// middleware
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// database connection
mongoose.connect("mongodb+srv://admin:admin@zuitt-course-booking.a1svtt9.mongodb.net/b244-to-do?retryWrites=true&w=majority",
		{
		useNewUrlParser : true,
		useUnifiedTopology : true
		}
	);

/*
 	Database Link

mongodb+srv://admin(change):admin(change)@zuitt-course-booking.a1svtt9.mongodb.net/(database_name)b244-to-do?retryWrites=true&w=majority


*/


// allows all the task routes created in the taskRoutes.js to use "/task" route

app.use("/tasks", taskRoute);

app.listen(port, () => {console.log(`Now listening to port ${port}`)});